# TP1 : (re)Familiaration avec un système GNU/Linux

## 0. Préparation de la machine

**Setup de deux machines Rocky Linux configurées de façon basique.**

**un accès internet (via la carte NAT)**

```
[etienne@node1 ~]$ ip a
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:b1:21:62 brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic noprefixroute enp0s3
       valid_lft 86301sec preferred_lft 86301sec
    inet6 fe80::a00:27ff:feb1:2162/64 scope link noprefixroute
       valid_lft forever preferred_lft forever

[etienne@node1 ~]$ ip neigh show
10.0.2.2 dev enp0s3 lladdr 52:54:00:12:35:02 REACHABLE
10.101.1.1 dev enp0s8 lladdr 0a:00:27:00:00:0d DELAY

[etienne@node2 ~]$ ip a
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:83:2a:57 brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic noprefixroute enp0s3
       valid_lft 86282sec preferred_lft 86282sec
    inet6 fe80::a00:27ff:fe83:2a57/64 scope link noprefixroute
       valid_lft forever preferred_lft forever

[etienne@node2 ~]$ ip neigh show
10.0.2.2 dev enp0s3 lladdr 52:54:00:12:35:02 REACHABLE
10.101.1.1 dev enp0s8 lladdr 0a:00:27:00:00:0d DELAY
```

**un accès à un réseau local**

```
[etienne@node1 ~]$ ip a
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:53:55:d1 brd ff:ff:ff:ff:ff:ff
    inet 10.101.1.11/24 brd 10.101.1.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe53:55d1/64 scope link
       valid_lft forever preferred_lft forever

[etienne@node1 ~]$ sudo cat /etc/sysconfig/network-scripts/ifcfg-enp0s8
DEVICE=enp0s8

BOOTPROTO=static
ONBOOT=yes

IPADDR=10.101.1.11
NETMASK=255.255.255.0

DNS1=1.1.1.1

[etienne@node2 ~]$ ip a
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:45:c0:d0 brd ff:ff:ff:ff:ff:ff
    inet 10.101.1.12/24 brd 10.101.1.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe45:c0d0/64 scope link
       valid_lft forever preferred_lft forever

[etienne@node2 ~]$ sudo cat /etc/sysconfig/network-scripts/ifcfg-enp0s8
DEVICE=enp0s8

BOOTPROTO=static
ONBOOT=yes

IPADDR=10.101.1.12
NETMASK=255.255.255.0

DNS1=1.1.1.1
```

**les machines doivent avoir un nom**

```
[etienne@node1 ~]$ sudo cat /etc/hostname
node1.tp1.b2

[etienne@node2 ~]$ sudo cat /etc/hostname
node2.tp1.b2
```

**utiliser 1.1.1.1 comme serveur DNS**

```
[etienne@node1 ~]$ dig ynov.com
;; SERVER: 1.1.1.1#53(1.1.1.1)

[etienne@node2 ~]$ dig ynov.com
;; SERVER: 1.1.1.1#53(1.1.1.1)
```

**les machines doivent pouvoir se joindre par leurs noms respectifs**

```
[etienne@node1 ~]$ sudo cat /etc/hosts
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
10.101.1.12 node2.tp1.b2

[etienne@node1 ~]$ ping node2.tp1.b2
PING node2.tp1.b2 (10.101.1.12) 56(84) bytes of data.
64 bytes from node2.tp1.b2 (10.101.1.12): icmp_seq=1 ttl=64 time=0.246 ms

[etienne@node2 ~]$ sudo cat /etc/hosts
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
10.101.1.11 node1.tp1.b2

[etienne@node2 ~]$ ping node1.tp1.b2
PING node1.tp1.b2 (10.101.1.11) 56(84) bytes of data.
64 bytes from node1.tp1.b2 (10.101.1.11): icmp_seq=1 ttl=64 time=0.248 ms
```

**le pare-feu est configuré pour bloquer toutes les connexions exceptées celles qui sont nécessaires**

```
[etienne@node1 ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports:
  protocols:
  forward: yes
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:

[etienne@node2 ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports:
  protocols:
  forward: yes
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

## I. Utilisateurs

**Ajouter un utilisateur à la machine,** 

```
[etienne@node1 ~]$ useradd unadmin -m -s /bin/bash -u 2000
[etienne@node1 ~]$ sudo cat /etc/passwd
unadmin:x:2000:2000::/home/unadmin:/bin/bash

[etienne@node2 ~]$ useradd unadmin -m -s /bin/bash -u 2000
[etienne@node2 ~]$ sudo cat /etc/passwd
unadmin:x:2000:2000::/home/unadmin:/bin/bash
```

**Créer un nouveau groupe admins**

```
[etienne@node1 ~]$ groupadd admin
[etienne@node1 ~]$ sudo visudo /etc/sudoers
[etienne@node1 ~]$ sudo cat /etc/sudoers
%admin ALL=(ALL)        ALL

[etienne@node2 ~]$ groupadd admin
[etienne@node2 ~]$ sudo visudo /etc/sudoers
[etienne@node2 ~]$ sudo cat /etc/sudoers
%admin ALL=(ALL)        ALL
```

**Ajouter votre utilisateur à ce groupe admins**

```
[etienne@node1 ~]$ usermod -aG admin unadmin
[unadmin@node1 etienne]$ sudo -l
Matching Defaults entries for unadmin on node1:
    !visiblepw, always_set_home, match_group_by_gid, always_query_group_plugin, env_reset, env_keep="COLORS DISPLAY
    HOSTNAME HISTSIZE KDEDIR LS_COLORS", env_keep+="MAIL PS1 PS2 QTDIR USERNAME LANG LC_ADDRESS LC_CTYPE",
    env_keep+="LC_COLLATE LC_IDENTIFICATION LC_MEASUREMENT LC_MESSAGES", env_keep+="LC_MONETARY LC_NAME LC_NUMERIC
    LC_PAPER LC_TELEPHONE", env_keep+="LC_TIME LC_ALL LANGUAGE LINGUAS _XKB_CHARSET XAUTHORITY",
    secure_path=/sbin\:/bin\:/usr/sbin\:/usr/bin

User unadmin may run the following commands on node1:

[etienne@node2 ~]$ usermod -aG admin unadmin
[unadmin@node2 etienne]$ sudo -l
Matching Defaults entries for unadmin on node2:
    !visiblepw, always_set_home, match_group_by_gid, always_query_group_plugin, env_reset, env_keep="COLORS DISPLAY
    HOSTNAME HISTSIZE KDEDIR LS_COLORS", env_keep+="MAIL PS1 PS2 QTDIR USERNAME LANG LC_ADDRESS LC_CTYPE",
    env_keep+="LC_COLLATE LC_IDENTIFICATION LC_MEASUREMENT LC_MESSAGES", env_keep+="LC_MONETARY LC_NAME LC_NUMERIC
    LC_PAPER LC_TELEPHONE", env_keep+="LC_TIME LC_ALL LANGUAGE LINGUAS _XKB_CHARSET XAUTHORITY",
    secure_path=/sbin\:/bin\:/usr/sbin\:/usr/bin

User unadmin may run the following commands on node2:
    (ALL) ALL
```

### 2. SSH

**Assurez vous que la connexion SSH est fonctionnelle**

```
PS C:\Users\etien> ssh etienne@10.101.1.11
Last login: Mon Nov 14 15:49:17 2022 from 10.101.1.1

PS C:\Users\etien> ssh etienne@10.101.1.12
Last login: Mon Nov 14 15:49:27 2022 from 10.101.1.1
```

## II. Partitionnement

### 1. Préparation de la VM

**Uniquement sur node1.tp1.b2**

```
[etienne@node1 ~]$ lsblk
sdb           8:16   0    3G  0 disk 
sdc           8:32   0    3G  0 disk
```

### 2. Partitionnement

```
[etienne@node1 ~]$ sudo vgcreate firstgroup /dev/sdb
  Physical volume "/dev/sdb" successfully created.
  Volume group "firstgroup" successfully created
[etienne@node1 ~]$ sudo vgextend firstgroup /dev/sdc
  Physical volume "/dev/sdc" successfully created.
  Volume group "firstgroup" successfully extended
[etienne@node1 ~]$ sudo vgs
  VG         #PV #LV #SN Attr   VSize VFree
  firstgroup   2   0   0 wz--n- 5.99g 5.99g
[etienne@node1 ~]$ sudo lvcreate -L 1G firstgroup -n firstpart
  Logical volume "firstpart" created.
[etienne@node1 ~]$ sudo lvcreate -L 1G firstgroup -n secondpart
  Logical volume "secondpart" created.
[etienne@node1 ~]$ sudo lvcreate -L 1G firstgroup -n thirdpart
  Logical volume "thirdpart" created.
```
```
[etienne@node1 ~]$ sudo mkfs -t ext4 /dev/firstgroup/firstpart
mke2fs 1.46.5 (30-Dec-2021)
Creating filesystem with 262144 4k blocks and 65536 inodes
Filesystem UUID: 2bfb5b26-d6f8-4656-b186-f0021c0d0c0b
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376

Allocating group tables: done
Writing inode tables: done
Creating journal (8192 blocks): done
Writing superblocks and filesystem accounting information: done

[etienne@node1 ~]$ sudo mkfs -t ext4 /dev/firstgroup/secondpart
mke2fs 1.46.5 (30-Dec-2021)
Creating filesystem with 262144 4k blocks and 65536 inodes
Filesystem UUID: 0e419c11-7d3c-4d98-a612-c042ef694e7c
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376

Allocating group tables: done
Writing inode tables: done
Creating journal (8192 blocks): done
Writing superblocks and filesystem accounting information: done

[etienne@node1 ~]$ sudo mkfs -t ext4 /dev/firstgroup/thirdpart
mke2fs 1.46.5 (30-Dec-2021)
Creating filesystem with 262144 4k blocks and 65536 inodes
Filesystem UUID: 2662cd6c-8af6-4b85-8c58-0a426ddc598a
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376

Allocating group tables: done
Writing inode tables: done
Creating journal (8192 blocks): done
Writing superblocks and filesystem accounting information: done
```
```
[etienne@node1 ~]$ sudo mkdir /mnt/part1
[etienne@node1 ~]$ sudo mkdir /mnt/part2
[etienne@node1 ~]$ sudo mkdir /mnt/part3

[etienne@node1 ~]$ sudo mount /dev/firstgroup/firstpart /mnt/part1
[etienne@node1 ~]$ sudo mount /dev/firstgroup/secondpart /mnt/part2
[etienne@node1 ~]$ sudo mount /dev/firstgroup/thirdpart /mnt/part3

[etienne@node1 ~]$ df -h
/dev/mapper/firstgroup-firstpart   974M   24K  907M   1% /mnt/part1
/dev/mapper/firstgroup-secondpart  974M   24K  907M   1% /mnt/part2
/dev/mapper/firstgroup-thirdpart   974M   24K  907M   1% /mnt/part3
```

**Grâce au fichier /etc/fstab**

```
[etienne@node1 ~]$ sudo nano /etc/fstab
[etienne@node1 ~]$ sudo cat /etc/fstab
/dev/firstgroup/firstpart /mnt/part1 ext4 defaults 0 0
/dev/firstgroup/secondpart /mnt/part2 ext4 defaults 0 0
/dev/firstgroup/thirdpart /mnt/part3 ext4 defaults 0 0
```
```
[etienne@node1 ~]$ sudo umount /mnt/part1
[etienne@node1 ~]$ sudo umount /mnt/part2
[etienne@node1 ~]$ sudo umount /mnt/part3
[etienne@node1 ~]$ sudo mount -av
/                        : ignored
/boot                    : already mounted
none                     : ignored
mount: /mnt/part1 does not contain SELinux labels.
       You just mounted a file system that supports labels which does not
       contain labels, onto an SELinux box. It is likely that confined
       applications will generate AVC messages and not be allowed access to
       this file system.  For more details see restorecon(8) and mount(8).
/mnt/part1               : successfully mounted
mount: /mnt/part2 does not contain SELinux labels.
       You just mounted a file system that supports labels which does not
       contain labels, onto an SELinux box. It is likely that confined
       applications will generate AVC messages and not be allowed access to
       this file system.  For more details see restorecon(8) and mount(8).
/mnt/part2               : successfully mounted
mount: /mnt/part3 does not contain SELinux labels.
       You just mounted a file system that supports labels which does not
       contain labels, onto an SELinux box. It is likely that confined
       applications will generate AVC messages and not be allowed access to
       this file system.  For more details see restorecon(8) and mount(8).
/mnt/part3               : successfully m
```

## III. Gestion de services

### 1. Interaction avec un service existant

```
[etienne@node1 ~]$ sudo systemctl status firewalld
     Active: active (running) since Mon 2022-11-14 18:12:31 CET; 24min ago
```

### 2. Création de service

#### A. Unité simpliste

**Créer un fichier qui définit une unité de service**

```
[etienne@node1 ~]$ sudo nano /etc/systemd/system/web.service
[etienne@node1 ~]$ sudo cat /etc/systemd/system/web.service
[Unit]
Description=Very simple web service

[Service]
ExecStart=/usr/bin/python3 -m http.server 8888

[Install]
WantedBy=multi-user.target

[etienne@node1 ~]$ sudo firewall-cmd --add-port=8888/tcp --permanent
success
[etienne@node1 ~]$ sudo firewall-cmd --reload
success
```
```
[etienne@node1 ~]$ sudo systemctl daemon-reload

[etienne@node1 ~]$ sudo systemctl status  web
○ web.service - Very simple web service
     Loaded: loaded (/etc/systemd/system/web.service; disabled; vendor preset: disabled)
     Active: inactive (dead)

[etienne@node1 ~]$ sudo systemctl start web
[etienne@node1 ~]$ sudo systemctl enable web
Created symlink /etc/systemd/system/multi-user.target.wants/web.service → /etc/systemd/system/web.service.
[etienne@node1 ~]$ sudo systemctl status web
     Active: active (running) since Mon 2022-11-14 18:43:40 CET; 13s ago
```

**Une fois le service démarré, assurez-vous que pouvez accéder au serveur web**

```
[etienne@node2 ~]$ curl 10.101.1.11:8888
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
```
#### B. Modification de l'unité

**Préparez l'environnement pour exécuter le mini serveur web Python**

```
[etienne@node1 ~]$ sudo mkdir /var/www
[etienne@node1 ~]$ sudo mkdir /var/www/meow
[etienne@node1 ~]$ sudo nano /var/www/meow/test
[etienne@node1 www]$ sudo chown web meow
[etienne@node1 www]$ ls -l
total 0
drwxr-xr-x. 2 web root 18 Nov 14 18:53 meow
[etienne@node1 meow]$ sudo chown web test
[etienne@node1 meow]$ ls -l
total 4
-rw-r--r--. 1 web root 7 Nov 14 18:53 test
```

**Modifiez l'unité de service web.service créée précédemment en ajoutant les clauses**

```
[etienne@node1 ~]$ sudo nano /etc/systemd/system/web.service
[etienne@node1 ~]$ sudo cat /etc/systemd/system/web.service
[Unit]
Description=Very simple web service

[Service]
ExecStart=/usr/bin/python3 -m http.server 8888
User=web
WorkingDirectory=/var/www/meow

[Install]
WantedBy=multi-user.target

[etienne@node1 ~]$ sudo systemctl daemon-reload
[etienne@node1 ~]$ sudo systemctl restart web
[etienne@node1 ~]$ sudo systemctl status web
     Active: active (running) since Mon 2022-11-14 19:02:02 CET; 3s ago
```

**Vérifiez le bon fonctionnement avec une commande curl**

```
[etienne@node2 ~]$ curl 10.101.1.11:8888
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Directory listing for /</title>
```
