# TP4 : Conteneurs

## I. Docker

### 1. Install

```
[etienne@docker1 ~]$ sudo dnf config-manager --add-repo=https://download.docker.com/linux/centos/docker-ce.repo
[sudo] password for etienne:
Adding repo from: https://download.docker.com/linux/centos/docker-ce.repo

[etienne@docker1 ~]$ sudo dnf update
Complete!

[etienne@docker1 ~]$ sudo dnf install docker-ce docker-ce-cli containerd.io
Complete!
```
```
[etienne@docker1 ~]$ sudo systemctl enable docker
Created symlink /etc/systemd/system/multi-user.target.wants/docker.service → /usr/lib/systemd/system/docker.service.
[etienne@docker1 ~]$ sudo systemctl start docker
[etienne@docker1 ~]$ sudo systemctl status docker
     Active: active (running) since Thu 2022-11-24 11:09:32 CET; 18s ago
```
```
[etienne@docker1 ~]$ sudo usermod -aG docker etienne
[etienne@docker1 ~]$ sudo reboot
[etienne@docker1 ~]$ docker info
Client:
 Context:    default
```

### 2. Vérifier l'install

```
[etienne@docker1 ~]$ docker info
Client:
 Context:    default
 Debug Mode: false

[etienne@docker1 ~]$ docker ps
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES

[etienne@docker1 ~]$ docker images
REPOSITORY   TAG       IMAGE ID   CREATED   SIZE

[etienne@docker1 ~]$ docker run debian
Unable to find image 'debian:latest' locally
latest: Pulling from library/debian
[etienne@docker1 ~]$ docker run -d debian sleep 99999
8264230daa5ec87cb6c0c6d96fe52472d3c67a31a806da7b28684c85237e4629
[etienne@docker1 ~]$ docker run -it debian bash
root@b15c8a9c3ed4:/#

[etienne@docker1 ~]$ docker ps
CONTAINER ID   IMAGE     COMMAND         CREATED          STATUS          PORTS     NAMES
8264230daa5e   debian    "sleep 99999"   44 seconds ago   Up 44 seconds             silly_williams
[etienne@docker1 ~]$ docker logs 8264230daa5e
[etienne@docker1 ~]$ docker logs -f 8264230daa5e

[etienne@docker1 ~]$ docker exec -it 8264230daa5e bash
root@8264230daa5e:/#
```
```
[etienne@docker1 ~]$ docker run --help

Usage:  docker run [OPTIONS] IMAGE [COMMAND] [ARG...]

Run a command in a new container

[etienne@docker1 ~]$ docker stop 8264230daa5e
8264230daa5e
[etienne@docker1 ~]$ docker ps
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES
```

### 3. Lancement de conteneurs

**Utiliser la commande docker run**

```
[etienne@docker1 ~]$ docker run --name web nginx
Unable to find image 'nginx:latest' locally
latest: Pulling from library/nginx

[etienne@docker1 ~]$ docker run --name web2 -v /path/to/html:/usr/share/nginx/html -p 8888:80 --cpus="1" --memory=500m n
ginx
/docker-entrypoint.sh: /docker-entrypoint.d/ is not empty, will attempt to perform configuration

[etienne@docker1 ~]$ sudo firewall-cmd --add-port=8888/tcp --permanent
success
[etienne@docker1 ~]$ sudo firewall-cmd --reload
success

PS C:\Users\etien> curl 10.104.1.11:8888


StatusCode        : 200
StatusDescription : OK
Content           : <!doctype html>
```

## II. Images

### 2. Construisez votre propre Dockerfile

**Construire votre propre image**

```
[etienne@docker1 fichierTravailDocker]$ nano apache2.conf
[etienne@docker1 fichierTravailDocker]$ cat apache2.conf
Listen 80
ServerName www.test2.com

LoadModule mpm_event_module "/usr/lib/apache2/modules/mod_mpm_event.so"
LoadModule dir_module "/usr/lib/apache2/modules/mod_dir.so"
LoadModule authz_core_module "/usr/lib/apache2/modules/mod_authz_core.so"

DirectoryIndex index.html
DocumentRoot "/var/www/html/"

ErrorLog "logs/error.log"
LogLevel warn

[etienne@docker1 fichierTravailDocker]$ nano dockerfile
[etienne@docker1 fichierTravailDocker]$ cat Dockerfile
FROM debian
RUN apt update -y
RUN apt install -y apache2
RUN rm /etc/apache2/apache2.conf
RUN mkdir /etc/apache2/logs
COPY apache2.conf /etc/apache2/
RUN rm /var/www/html/index.html
COPY index.html /var/www/html/index.html
CMD ["apache2ctl", "-D", "FOREGROUND"]
[etienne@docker1 fichierTravailDocker]$ docker build . -t apache_serve
[etienne@docker1 fichierTravailDocker]$ docker run -p 80:80 apache_serve
PS C:\Users\etien> curl 10.104.1.11:80                                                                                  

StatusCode        : 200
StatusDescription : OK
PS C:\Users\etien> scp etienne@10.104.1.11:/home/etienne/fichierTravailDocker/Dockerfile Dockerfile
Dockerfile
```

**Dockerfile**

void fichier **Dockerfile**

## III. docker-compose

### 1. Intro

```
[etienne@docker1 ~]$ sudo dnf config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
Adding repo from: https://download.docker.com/linux/centos/docker-ce.repo
[etienne@docker1 ~]$ sudo dnf install docker-compose-plugin
Complete!
```

### 2. Make your own meow

**Conteneurisez votre application**

```
[etienne@docker1 ~]$ sudo dnf install git -y    
[etienne@docker1 ~]$ git clone https://github.com/LGARRABOS/nmap_project.git
[etienne@docker1 ~]$ mkdir mon_moew
[etienne@docker1 ~]$ cd mon_moew
[etienne@docker1 mon_moew]$ nano Dockerfile
[etienne@docker1 mon_moew]$ cat Dockerfile
FROM python:3

WORKDIR /usr/src/app

RUN pip install --upgrade pip
RUN pip install scapy netifaces netaddr

COPY ./nmap_project .

CMD [ "python", "./scan.py" ]
```
```
[etienne@docker1 mon_moew]$ nano docker-compose.yml
[etienne@docker1 mon_moew]$ cat docker-compose.yml
version: "3"
services:
  redis:
    image: "scan"

[etienne@docker1 mon_moew]$ docker build . -t scan
[etienne@docker1 mon_moew]$ docker compose up
[+] Running 2/1
 ⠿ Network mon_moew_default    Created                                                                             0.3s
 ⠿ Container mon_moew-redis-1  Created                                                                             0.0s
Attaching to mon_moew-redis-1
mon_moew-redis-1  | Invalid command
mon_moew-redis-1  | This program is a network scanner
mon_moew-redis-1  | You have to run this program as root
mon_moew-redis-1  | Programm command list:
mon_moew-redis-1  |  -h  --help        Gives access to the list of commands and their uses.
mon_moew-redis-1  |  -a  --arp         Make a ARP ping request on all the whole network and write result in file. You can specify the interface you want to scan in argument.
mon_moew-redis-1  |  -t  --tcp         Gives from a list of ports the services that listen behind. You can specify the Ip you want to scan in argument.
mon_moew-redis-1  |  -os               Make a request to a specific Ip and return the os. You can specify the Ip you want to scan in argument.
mon_moew-redis-1  |  -p  --print       Print Save of specific interfaces. You can specify the interface you want to scan in argument.
mon_moew-redis-1 exited with code 0
```

voir dossier **app**




