# TP2 : Gestion de service

## I. Un premier serveur web

### 1. Installation

**Installer le serveur Apache**

```
[etienne@web ~]$ sudo dnf install httpd
Complete!

[etienne@web ~]$ sudo vim /etc/httpd/conf/httpd.conf
[etienne@web ~]$ sudo cat /etc/httpd/conf/httpd.conf

ServerRoot "/etc/httpd"

Listen 80
```

**Démarrer le service Apache**

```
[etienne@web ~]$ sudo systemctl start httpd
[etienne@web ~]$ sudo systemctl status httpd
     Active: active (running) since Tue 2022-11-15 09:58:42 CET; 6s ago
[etienne@web ~]$ sudo systemctl enable httpd
Created symlink /etc/systemd/system/multi-user.target.wants/httpd.service → /usr/lib/systemd/system/httpd.service.
```
```
[etienne@web ~]$ sudo ss -tulpn | grep LISTEN
tcp   LISTEN 0      511                *:80              *:*    users:(("httpd",pid=32849,fd=4),("httpd",pid=32848,fd=4),("httpd",pid=32847,fd=4),("httpd",pid=32845,fd=4))

[etienne@web ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
```

**TEST**

```
[etienne@web ~]$ sudo systemctl status httpd
     Active: active (running) since Tue 2022-11-15 10:09:48 CET; 7s ago
```
```
[etienne@web ~]$ curl localhost
<!doctype html>
<html>
  <head>
```
```
PS C:\Users\etien> curl 10.102.1.11:80
curl : HTTP Server Test Page
```

(windows c'est nul sont curl reconnait une erreur mais sa marche)

### 2. Avancer vers la maîtrise du service

**Le service Apache...**

```
[etienne@web ~]$ sudo cat /etc/systemd/system/multi-user.target.wants/httpd.service
[Unit]
Description=The Apache HTTP Server
Wants=httpd-init.service
After=network.target remote-fs.target nss-lookup.target httpd-init.service
Documentation=man:httpd.service(8)

[Service]
Type=notify
Environment=LANG=C

ExecStart=/usr/sbin/httpd $OPTIONS -DFOREGROUND
ExecReload=/usr/sbin/httpd $OPTIONS -k graceful
# Send SIGWINCH for graceful stop
KillSignal=SIGWINCH
KillMode=mixed
PrivateTmp=true
OOMPolicy=continue

[Install]
WantedBy=multi-user.target
```

**Déterminer sous quel utilisateur tourne le processus Apache**

```
[etienne@web ~]$ sudo cat /etc/httpd/conf/httpd.conf
User apache
```
```
[etienne@web ~]$ ps -ef
apache     33136   33135  0 10:09 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache     33137   33135  0 10:09 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache     33138   33135  0 10:09 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache     33139   33135  0 10:09 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
```
```
[etienne@web ~]$ cd /usr/share/testpage
[etienne@web testpage]$ ls -al
-rw-r--r--.  1 root root 7620 Jul  6 04:37 index.html
```

**Changer l'utilisateur utilisé par Apache**

```
[etienne@web ~]$ sudo cat /etc/passwd
apache:x:48:48:Apache:/usr/share/httpd:/sbin/nologin
```
```
[etienne@web ~]$ sudo useradd newuser2 -m -s /sbin/nologin
[etienne@web ~]$ sudo cat /etc/passwd
newuser2:x:1001:1002::/home/newuser2:/sbin/nologin
```
```
[etienne@web ~]$ sudo nano /etc/httpd/conf/httpd.conf
[etienne@web ~]$ sudo cat /etc/httpd/conf/httpd.conf
User newuser2
```
```
[etienne@web ~]$ sudo systemctl restart httpd
[etienne@web ~]$ ps -ef
newuser2   33491   33490  0 10:43 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
newuser2   33492   33490  0 10:43 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
newuser2   33493   33490  0 10:43 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
newuser2   33494   33490  0 10:43 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
```

**Faites en sorte que Apache tourne sur un autre port**

```
[etienne@web ~]$ sudo nano /etc/httpd/conf/httpd.conf
[etienne@web ~]$ sudo cat /etc/httpd/conf/httpd.conf
Listen 8080
```
```
[etienne@web ~]$ sudo firewall-cmd --add-port=8080/tcp --permanent
success
[etienne@web ~]$ sudo firewall-cmd --reload
success
```
```
[etienne@web ~]$ sudo systemctl restart httpd
[etienne@web ~]$ sudo ss -tulpn | grep LISTEN
tcp   LISTEN 0      511                *:8080            *:*    users:(("httpd",pid=33732,fd=4),("httpd",pid=33731,fd=4),("httpd",pid=33730,fd=4),("httpd",pid=33728,fd=4))
```
```
[etienne@web ~]$ curl localhost:8080
<!doctype html>
<html>
  <head>
```
```
etien@DESKTOP-CCLQQ92 MINGW64 ~
$ curl 10.102.1.11:8080
<html>
  <head>
```

**Fichier /etc/httpd/conf/httpd.conf**

voir fichier **fichier_conf**

## II. Une stack web plus avancée

**Réinitialiser votre conf Apache avant de continuer**

```
[etienne@web ~]$ sudo cat /etc/httpd/conf/httpd.conf
Listen 80
User apache
```

### 2. Setup

#### A. Base de données

**Install de MariaDB sur db.tp2.linux**

```
[etienne@db ~]$ sudo dnf install mariadb-server -y
Complete!
```
```
[etienne@db ~]$ sudo systemctl enable mariadb
Created symlink /etc/systemd/system/mysql.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/mysqld.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/multi-user.target.wants/mariadb.service → /usr/lib/systemd/system/mariadb.service.
```
```
[etienne@db ~]$ sudo systemctl start mariadb
[etienne@db ~]$ sudo systemctl status mariadb
     Active: active (running) since Tue 2022-11-15 11:29:56 CET; 12s ago
```
```
[etienne@db ~]$ sudo mysql_secure_installation
Switch to unix_socket authentication [Y/n] y
Change the root password? [Y/n] y
Remove anonymous users? [Y/n] y
Disallow root login remotely? [Y/n] y
Remove test database and access to it? [Y/n] y
Reload privilege tables now? [Y/n] y
```
```
[etienne@db ~]$ sudo ss -tulpn | grep LISTEN
tcp   LISTEN 0      80                 *:3306            *:*    users:(("mariadbd",pid=3115,fd=18))
[etienne@db ~]$ sudo firewall-cmd --add-port=3306/tcp --permanent
success
[etienne@db ~]$ sudo firewall-cmd --reload
success
```

**Préparation de la base pour NextCloud**

```
[etienne@db ~]$ sudo mysql -u root -p

MariaDB [(none)]> CREATE USER 'nextcloud'@'10.102.1.11' IDENTIFIED BY 'pewpewpew';
Query OK, 0 rows affected (0.001 sec)

MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
Query OK, 1 row affected (0.000 sec)

MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.102.1.11';
Query OK, 0 rows affected (0.002 sec)

MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.001 sec)
```

**Exploration de la base de données**

```
[etienne@web ~]$ sudo dnf install mysql
Complete!
```
```
[etienne@web ~]$ sudo mysql -u nextcloud -h 10.102.1.12 -p

mysql> show DATABASES;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| nextcloud          |
+--------------------+
2 rows in set (0.00 sec)
```
```
mysql> USE information_schema
Reading table information for completion of table and column names
You can turn off this feature to get a quicker startup with -A

Database changed
```
```
mysql> SHOW TABLES;
+---------------------------------------+
| Tables_in_information_schema          |
+---------------------------------------+
```

**Trouver une commande SQL qui permet de lister tous les utilisateurs de la base de donnée**

```
MariaDB [(none)]> select user, host from mysql.user;
+-------------+-------------+
| User        | Host        |
+-------------+-------------+
| newtcloud   | 10.102.1.11 |
| nextcloud   | 10.102.1.11 |
| mariadb.sys | localhost   |
| mysql       | localhost   |
| root        | localhost   |
+-------------+-------------+
5 rows in set (0.001 sec)
```

#### B. Serveur Web et NextCloud

**Install de PHP**

```
[etienne@web ~]$ sudo dnf config-manager --set-enabled crb
[etienne@web ~]$ sudo dnf install dnf-utils http://rpms.remirepo.net/enterprise/remi-release-9.rpm -y
Complete!
```
```
[etienne@web ~]$ dnf module list php
php          remi-8.2         common [d], devel, minimal         PHP scripting language

Hint: [d]efault, [e]nabled, [x]disabled, [i]nstalled

[etienne@web ~]$ sudo dnf module enable php:remi-8.1 -y
Complete!

[etienne@web ~]$ sudo dnf install -y php81-php
Complete!

[etienne@web ~]$  sudo dnf install -y libxml2 openssl php81-php php81-php-ctype php81-php-curl php81-php-gd php81-php-iconv php81-php-json php81-php-libxml php81-php-mbstring php81-php-openssl php81-php-posix php81-php-session php81-php-xml php81-php-zip php81-php-zlib php81-php-pdo php81-php-mysqlnd php81-php-intl php81-php-bcmath php81-php-gmp
Complete!
```

**Récupérer NextCloud**

```
[etienne@web ~]$ sudo mkdir /var/www/tp2_nextcloud
[etienne@web ~]$ cd /var/www/tp2_nextcloud
[etienne@web tp2_nextcloud]$
```
```
[etienne@web www]$ sudo dnf install wget
Complete!
[etienne@web www]$ sudo wget https://download.nextcloud.com/server/prereleases/nextcloud-25.0.0rc3.zip
[etienne@web www]$ sudo dnf install unzip
Complete!
[etienne@web www]$ sudo unzip nextcloud-25.0.0rc3.zip
inflating: nextcloud/config/.htaccess
```
```
[etienne@web www]$ sudo rm -r nextcloud-25.0.0rc3.zip
[etienne@web www]$ sudo rm -r tp2_nextcloud/
[etienne@web www]$ ls
cgi-bin  html  nextcloud
```
```
[etienne@web www]$ sudo mv nextcloud tp2_nextcloud
[etienne@web www]$ ls
cgi-bin  html  tp2_nextcloud
```
```
[etienne@web tp2_nextcloud]$ sudo chown -R apache:apache /var/www/tp2_nextcloud
[etienne@web tp2_nextcloud]$ ls -al
-rw-r--r--.  1 apache apache   156 Oct  6 14:42 index.html
[etienne@web www]$ ls -al
drwxr-xr-x. 14 apache apache 4096 Oct  6 14:47 tp2_nextcloud
```

**Adapter la configuration d'Apache**

```
[etienne@web conf.d]$ sudo nano /etc/httpd/conf.d/nextcloud.conf
[etienne@web conf.d]$ sudo cat /etc/httpd/conf.d//nextcloud.conf
<VirtualHost *:80>
  DocumentRoot /var/www/tp2_nextcloud/
  ServerName  web.tp2.linux
  <Directory /var/www/tp2_nextcloud/>
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews
    <IfModule mod_dav.c>
      Dav off
    </IfModule>
  </Directory>
</VirtualHost>
```

**Redémarrer le service Apache**

```
[etienne@web conf.d]$ sudo systemctl restart httpd
[etienne@web conf.d]$ sudo systemctl status httpd
     Active: active (running) since Tue 2022-11-15 12:36:15 CET; 12s ago
```

#### C. Finaliser l'installation de NextCloud

**Exploration de la base de données**

```
[etienne@db ~]$ sudo mysql -u root
Welcome to the MariaDB monitor.  Commands end with ; or \g.
```
```
MariaDB [(none)]> use nextcloud;
Reading table information for completion of table and column names

MariaDB [(none)]> SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'nextclo
ud';
+----------+
| COUNT(*) |
+----------+
|       95 |
+----------+
1 row in set (0.001 sec)`
```
