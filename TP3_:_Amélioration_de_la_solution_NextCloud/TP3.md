# TP3 : Amélioration de la solution NextCloud

## Module 1 : Reverse Proxy

### II. Setup

**On utilisera NGINX comme reverse proxy**

```
[etienne@proxy ~]$ sudo dnf install nginx
Complete!
```
```
[etienne@proxy ~]$ sudo systemctl start nginx
[etienne@proxy ~]$ sudo systemctl status nginx
     Active: active (running) since Thu 2022-11-17 10:51:31 CET; 6s ago
[etienne@proxy ~]$ sudo systemctl enable nginx
Created symlink /etc/systemd/system/multi-user.target.wants/nginx.service → /usr/lib/systemd/system/nginx.service.
```
```
[etienne@proxy ~]$ sudo ss -tulpn | grep LISTEN
tcp   LISTEN 0      511          0.0.0.0:80        0.0.0.0:*    users:(("nginx",pid=1033,fd=6),("nginx",pid=1032,fd=6))
tcp   LISTEN 0      511             [::]:80           [::]:*    users:(("nginx",pid=1033,fd=7),("nginx",pid=1032,fd=7))
```
```
[etienne@proxy ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
[etienne@proxy ~]$ sudo firewall-cmd --reload
success
```
```
[etienne@proxy ~]$ ps -ef
root        1032       1  0 10:51 ?        00:00:00 nginx: master process /usr/sbin/nginx
nginx       1033    1032  0 10:51 ?        00:00:00 nginx: worker process
```
```
[etienne@proxy ~]$ curl localhost
<!doctype html>
<html>
[etienne@web ~]$ curl 10.102.1.13
<!doctype html>
<html>
```

**Configurer NGINX**

```
[etienne@proxy nginx]$ sudo cat nginx.conf
    include /etc/nginx/conf.d/*.conf;
[etienne@proxy ~]$ sudo nano /etc/nginx/conf.d/maconf.conf
[etienne@proxy ~]$ sudo cat /etc/nginx/conf.d/maconf.conf
server {
    server_name web.tp2.linux;

    listen 80;

    location / {
        proxy_set_header  Host $host;
        proxy_set_header  X-Real-IP $remote_addr;
        proxy_set_header  X-Forwarded-Proto https;
        proxy_set_header  X-Forwarded-Host $remote_addr;
        proxy_set_header  X-Forwarded-For $proxy_add_x_forwarded_for;

        proxy_pass http://10.102.1.11:80;
    }

    location /.well-known/carddav {
      return 301 $scheme://$host/remote.php/dav;
    }

    location /.well-known/caldav {
      return 301 $scheme://$host/remote.php/dav;
    }
}
[etienne@proxy ~]$ sudo systemctl restart nginx
[etienne@proxy ~]$ sudo systemctl status nginx
     Active: active (running) since Thu 2022-11-17 11:11:33 CET; 42s ago
```
```
[etienne@web /]$ sudo find . -name config.php
./var/www/tp2_nextcloud/config/config.php
[etienne@web /]$ sudo nano ./var/www/tp2_nextcloud/config/config.php
[etienne@web /]$ sudo cat ./var/www/tp2_nextcloud/config/config.php
  'trusted_domains' =>
  array (
    0 => '10.102.1.13',
  ),
  'proxy' => '10.102.1.13:80',
[etienne@web /]$ sudo systemctl restart httpd
[etienne@web /]$ sudo systemctl stataus httpd
Unknown command verb stataus.
[etienne@web /]$ sudo systemctl status httpd
     Active: active (running) since Thu 2022-11-17 11:19:05 CET; 16s ago
[etienne@web /]$ curl 10.102.1.13
<!DOCTYPE html>
<html class="ng-csp" data-placeholder-focus="false" lang="en" data-locale="en" >
```

**Bonus**

```
[etienne@web ~]$ sudo firewall-cmd --permanent --add-rich-rule='rule family="ipv4" source address="10.102.1.1" service name=ssh accept'
success
[etienne@web ~]$ sudo firewall-cmd --permanent --add-rich-rule='rule family="ipv4" source address="10.102.1.13" port port=80 protocol=tcp accept'
success
[etienne@web ~]$ sudo firewall-cmd --permanent --add-rich-rule='rule protocol value=icmp reject'
success
[etienne@web ~]$ sudo firewall-cmd --reload
success
[etienne@web ~]$ sudo firewall-cmd --list-all
  rich rules:
        rule family="ipv4" source address="10.102.1.1" service name="ssh" accept
        rule family="ipv4" source address="10.102.1.13" port port="80" protocol="tcp" accept
        rule protocol value="icmp" reject
```
```
PS C:\Users\etien> curl 10.102.1.13


StatusCode        : 200
StatusDescription : OK

PS C:\Users\etien> curl 10.102.1.11
```
quand on veux aller directement sur la machine **10.102.1.11** on **tourne dans le vide** et on est **rejeté**.

### III. HTTPS

```
[etienne@proxy ssl]$ sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/nginx-selfsigned.key -out /etc/ssl/certs/nginx-selfsigned.crt

You are about to be asked to enter information that will be incorporated
into your certificate request.

[etienne@proxy ssl]$ sudo openssl dhparam -out /etc/nginx/dhparam.pem 4096
Generating DH parameters, 4096 bit long safe prime
```
```
[etienne@proxy ~]$ sudo nano /etc/nginx/sites-available/web.tp2.linux
[etienne@proxy ~]$ sudo cat /etc/nginx/sites-available/web.tp2.linux
server {
        listen 80;
        listen [::]:80;

        root /var/www/web.tp2.linux/html;
        index index.html index.htm index.nginx-debian.html;

        server_name web.tp2.linux www.web.tp2.linux;

        location / {
                try_files $uri $uri/ =404;
        }
}
```
```
[etienne@proxy conf.d]$ sudo nano /etc/nginx/conf.d/maconf.conf
[etienne@proxy conf.d]$ sudo cat /etc/nginx/conf.d/maconf.conf
server {
    server_name web.tp2.linux;


    listen 443 ssl;
    listen [::]:443 ssl;
    include snippets/self-signed.conf;
    include snippets/ssl-params.conf;
```
```
[etienne@proxy ~]$ sudo nano /etc/nginx/sites-available/web.tp2.linux
[etienne@proxy ~]$ sudo cat /etc/nginx/sites-available/web.tp2.linux
server {
    listen 443 ssl;
    listen [::]:443 ssl;
    include snippets/self-signed.conf;
    include snippets/ssl-params.conf;

root /var/www/your_domain/html;
        index index.html index.htm index.nginx-debian.html;

  server_name your_domain.com www.your_domain.com;

  location / {
                try_files $uri $uri/ =404;
        }
}
server {
    listen 80;
    listen [::]:80;

    server_name web.tp2.linux.com www.web.tp2.linux.com;

    return 302 https://$server_name$request_uri;
}
```
```
[etienne@proxy ~]$ sudo firewall-cmd --add-port=443/tcp --permanent
success
[etienne@proxy ~]$ sudo firewall-cmd --reload
success
```
```
[etienne@proxy ~]$ sudo nginx -t
nginx: [warn] "ssl_stapling" ignored, issuer certificate not found for certificate "/etc/ssl/certs/nginx-selfsigned.crt"
nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
nginx: configuration file /etc/nginx/nginx.conf test is successful
[etienne@proxy ~]$ sudo systemctl restart nginx
[etienne@proxy ~]$ sudo systemctl status nginx
     Active: active (running) since Fri 2022-11-18 11:16:09 CET; 7s ago
```
```
[etienne@proxy ~]$ sudo nano /etc/nginx/sites-available/web.tp2.linux
[etienne@proxy ~]$ sudo cat /etc/nginx/sites-available/web.tp2.linux
    return 301 https://$server_name$request_uri;
[etienne@proxy ~]$ sudo nginx -t
nginx: [warn] "ssl_stapling" ignored, issuer certificate not found for certificate "/etc/ssl/certs/nginx-selfsigned.crt"
nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
nginx: configuration file /etc/nginx/nginx.conf test is successful
[etienne@proxy ~]$ sudo systemctl restart nginx
```
```
[etienne@web ~]$ sudo nano /var/www/tp2_nextcloud/config/config.php
[sudo] password for etienne:
[etienne@web ~]$ sudo cat /var/www/tp2_nextcloud/config/config.php
  'overwriteprotocol' => 'https',
```

## Module 7 : Fail2Ban

```
[etienne@proxy ~]$ sudo dnf install epel-release -y
Complete!
[etienne@proxy ~]$ sudo dnf install fail2ban -y
Complete!
```
```
[etienne@proxy ~]$ sudo systemctl start fail2ban.service
[etienne@proxy ~]$ sudo systemctl enable fail2ban.service
Created symlink /etc/systemd/system/multi-user.target.wants/fail2ban.service → /usr/lib/systemd/system/fail2ban.service.
[etienne@proxy ~]$ sudo systemctl status fail2ban.service
     Active: active (running) since Fri 2022-11-18 11:32:59 CET; 1min 12s ago
```
```
[etienne@proxy ~]$ cd /etc/fail2ban
[etienne@proxy ~]$ sudo nano /etc/fail2ban/jail.conf
[etienne@proxy ~]$ sudo cat /etc/fail2ban/jail.conf
# "bantime" is the number of seconds that a host is banned.
bantime  = 1d

# A host is banned if it has generated "maxretry" during the last "findtime"
# seconds.
findtime  = 1m

# "maxretry" is the number of failures before a host get banned.
maxretry = 3
[...]

[sshd]

# To use more aggressive sshd modes set filter parameter "mode" in jail.local:
# normal (default), ddos, extra or aggressive (combines all).
# See "tests/files/logs/sshd" or "filter.d/sshd.conf" for usage example and details.
#mode   = normal
enabled = true
port    = ssh
logpath = %(sshd_log)s
backend = %(sshd_backend)s
```
```
[etienne@proxy ~]$ sudo systemctl restart fail2ban.service
[etienne@proxy ~]$ sudo systemctl status fail2ban.service
     Active: active (running) since Fri 2022-11-18 11:44:41 CET; 11s ago
```
```
[etienne@faitToiBan ~]$ ssh etienne@10.102.1.13
etienne@10.102.1.13's password:
etienne@10.102.1.13: Permission denied (publickey,gssapi-keyex,gssapi-with-mic,password).
[etienne@faitToiBan ~]$ ssh etienne@10.102.1.13
ssh: connect to host 10.102.1.13 port 22: Connection refused
[etienne@proxy ~]$ sudo fail2ban-client status sshd
`- Actions
   |- Currently banned: 1
   |- Total banned:     1
   `- Banned IP list:   10.102.1.14
```
```
[etienne@proxy ~]$ sudo fail2ban-client unban 10.102.1.14
[etienne@proxy ~]$ sudo fail2ban-client status sshd
`- Actions
   |- Currently banned: 0
   |- Total banned:     1
   `- Banned IP list:
```

## Module 5 : Monitoring

**Je vous laisse suivre la doc pour le mettre en place**

```
[etienne@db ~]$ sudo wget -O /tmp/netdata-kickstart.sh https://my-netdata.io/kickstart.sh && sh /tmp/netdata-kickstart.sh
Complete!

[etienne@db ~]$ sudo [ "703f0c06ff9f1c47c11b99907d0b95d2" = "$(curl -Ss https://my-netdata.io/kickstart.sh | md5sum | cut -d ' ' -f 1)" ] && echo "OK, VALID" || echo "FAILED, INVALID"
OK, VALID

[etienne@db ~]$ sudo systemctl start netdata
[etienne@db ~]$ sudo systemctl enable netdata
[etienne@db ~]$ sudo systemctl status netdata
     Active: active (running) since Mon 2022-11-21 09:09:59 CET; 1h 41min ago

[etienne@db ~]$ sudo firewall-cmd --permanent --add-port=19999/tcp
success
[etienne@db ~]$ sudo firewall-cmd --reload
success

[etienne@web ~]$ sudo wget -O /tmp/netdata-kickstart.sh https://my-netdata.io/kickstart.sh && sh /tmp/netdata-kickstart.sh
Complete!

[etienne@web ~]$ [ "703f0c06ff9f1c47c11b99907d0b95d2" = "$(curl -Ss https://my-netdata.io/kickstart.sh | md5sum | cut -d ' ' -f 1)" ] && echo "OK, VALID" || echo "FAILED, INVALID"
OK, VALID

[etienne@web ~]$ sudo systemctl start netdata
[etienne@web ~]$ sudo systemctl enable netdata
[etienne@web ~]$ sudo systemctl status netdata
     Active: active (running) since Mon 2022-11-21 09:06:30 CET; 1h 43min ago

[etienne@web ~]$ sudo firewall-cmd --permanent --add-port=19999/tcp
success
[etienne@web ~]$ sudo firewall-cmd --reload
success
```

```
[etienne@web ~]$ sudo nano /etc/netdata/health_alarm_notify.conf
[etienne@web ~]$ sudo cat /etc/netdata/health_alarm_notify.conf
###############################################################################
# sending discord notifications

# note: multiple recipients can be given like this:
#                  "CHANNEL1 CHANNEL2 ..."

# enable/disable sending discord notifications
SEND_DISCORD="YES"

# Create a webhook by following the official documentation -
# https://support.discordapp.com/hc/en-us/articles/228383668-Intro-to-Webhooks
DISCORD_WEBHOOK_URL="https://discord.com/api/webhooks/1044189884329508874/hXQkQ5J8Iu7wy2iwDF4Ayr1KeAjJrgpLumfKe22g6g3AEWKURruT24tkr6Tx8etCdFVc"

# if a role's recipients are not configured, a notification will be send to
# this discord channel (empty = do not send a notification for unconfigured
# roles):
DEFAULT_RECIPIENT_DISCORD="alarms"

[etienne@db ~]$ sudo nano /etc/netdata/health_alarm_notify.conf
[etienne@db ~]$ sudo cat /etc/netdata/health_alarm_notify.conf
# sending discord notifications

# note: multiple recipients can be given like this:
#                  "CHANNEL1 CHANNEL2 ..."

# enable/disable sending discord notifications
SEND_DISCORD="YES"

# Create a webhook by following the official documentation -
# https://support.discordapp.com/hc/en-us/articles/228383668-Intro-to-Webhooks
DISCORD_WEBHOOK_URL="https://discord.com/api/webhooks/1044189884329508874/hXQkQ5J8Iu7wy2iwDF4Ayr1KeAjJrgpLumfKe22g6g3AEWKURruT24tkr6Tx8etCdFVc"

# if a role's recipients are not configured, a notification will be send to
# this discord channel (empty = do not send a notification for unconfigured
# roles):
DEFAULT_RECIPIENT_DISCORD="alarms"
```
```
[etienne@web ~]$ sudo nano /etc/netdata/health.d/cpu_usage.conf
[etienne@web ~]$ sudo cat /etc/netdata/health.d/cpu_usage.conf
alarm: cpu_usage
on: system.cpu
lookup: average -3s percentage foreach user,system
units: %
every: 10s
warn: $this > 50
crit: $this > 80
info: CPU utilization of users on the system itself.

[etienne@web ~]$ sudo systemctl restart netdata
[etienne@web ~]$ sudo systemctl status netdata
     Active: active (running) since Mon 2022-11-21 11:10:45 CET; 5s ago

[etienne@db ~]$ sudo cat /etc/netdata/health.d/cpu_usage.conf
alarm: cpu_usage
on: system.cpu
lookup: average -3s percentage foreach user,system
units: %
every: 10s
warn: $this > 50
crit: $this > 80
info: CPU utilization of users on the system itself.

[etienne@db ~]$ sudo systemctl restart netdata
[etienne@db ~]$ sudo systemctl status netdata
     Active: active (running) since Mon 2022-11-21 11:10:25 CET; 6s ago
```
```
[etienne@web ~]$ sudo dnf install stress
Complete!

[etienne@web ~]$ stress -c 2 -i 1 -m 1 --vm-bytes 128M -t 10s
stress: info: [3855] dispatching hogs: 2 cpu, 1 io, 1 vm, 0 hdd
stress: info: [3855] successful run completed in 10s
```

## Module 2 : Réplication de base de données

```
[etienne@localhost ~]$ echo 'db2.tp3.linux' | sudo tee /etc/hostname
[etienne@db2 ~]$ sudo dnf install mariadb-server -y
Complete!

[etienne@db2 ~]$ sudo systemctl start mariadb
[etienne@db2 ~]$ sudo systemctl enable mariadb
Created symlink /etc/systemd/system/mysql.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/mysqld.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/multi-user.target.wants/mariadb.service → /usr/lib/systemd/system/mariadb.service.
[etienne@db2 ~]$ sudo systemctl status mariadb
     Active: active (running) since Mon 2022-11-21 11:27:03 CET; 20s ago

[etienne@db2 ~]$ sudo mysql_secure_installation
Switch to unix_socket authentication [Y/n] y
Change the root password? [Y/n] y
Remove anonymous users? [Y/n] y
Disallow root login remotely? [Y/n] y
Remove test database and access to it? [Y/n] y
Reload privilege tables now? [Y/n] y
```
```
[etienne@db ~]$ sudo nano /etc/my.cnf.d/mariadb-server.cnf
[etienne@db ~]$ sudo cat /etc/my.cnf.d/mariadb-server.cnf
[mariadb]
log-bin
server_id=1
log-basename=master1
binlog-format=mixed

[etienne@db ~]$ sudo systemctl restart mariadb
[etienne@db ~]$ ss -antpl | grep LISTEN
LISTEN 0      80           0.0.0.0:3306       0.0.0.0:*
```
```
[etienne@db ~]$ sudo mysql -u root
MariaDB [(none)]> CREATE USER 'replication'@'10.102.1.14' identified by 'tototo';
Query OK, 0 rows affected (0.002 sec)

MariaDB [(none)]> GRANT REPLICATION SLAVE ON *.* TO 'replication'@'10.102.1.14';
Query OK, 0 rows affected (0.001 sec)

MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.001 sec)

MariaDB [(none)]> SHOW MASTER STATUS;
+--------------------+----------+--------------+------------------+
| File               | Position | Binlog_Do_DB | Binlog_Ignore_DB |
+--------------------+----------+--------------+------------------+
| master1-bin.000002 |      809 |              |                  |
+--------------------+----------+--------------+------------------+
1 row in set (0.000 sec)
```
```
[etienne@db2 ~]$ sudo nano /etc/my.cnf.d/mariadb-server.cnf
[etienne@db2 ~]$ sudo cat /etc/my.cnf.d/mariadb-server.cnf
[mariadb]
log-bin
server_id=2
log-basename=slave1
binlog-format=mixed
[etienne@db2 ~]$ sudo systemctl restart mariadb
```
```
[etienne@db2 ~]$ sudo mysql -u root -p
MariaDB [(none)]> STOP SLAVE;
Query OK, 0 rows affected, 1 warning (0.000 sec)

MariaDB [(none)]>  CHANGE MASTER TO MASTER_HOST = '10.102.1.12', MASTER_USER = 'replication', MASTER_PASSWORD = 'tototo'
, MASTER_LOG_FILE = 'master1-bin.000002', MASTER_LOG_POS = 809;
Query OK, 0 rows affected (0.013 sec)

MariaDB [(none)]> START SLAVE;
Query OK, 0 rows affected (0.002 sec)
```
```
[etienne@db ~]$ sudo mysqldump -u root nextcloud > toto.sql
[etienne@db ~]$ cat toto.sql
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-11-21 12:24:24

[etienne@db ~]$ scp toto.sql etienne@10.102.1.14:/tmp/
[etienne@db2 ~]$ sudo mv /tmp/toto.sql ./
[etienne@db2 ~]$ sudo mysql -u root < toto.sql
[etienne@db2 ~]$ sudo mysql -u root

[etienne@db2 ~]$ sudo vim toto.sql
CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
use nextcloud;

MariaDB [(none)]> show databases ;
+--------------------+
| Database           |
+--------------------+
| etienneetluc       |
| information_schema |
| mysql              |
| nextcloud          |
| performance_schema |
+--------------------+
5 rows in set (0.000 sec)
```
